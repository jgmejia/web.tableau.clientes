//  To prevent a bug in IE where the global use of the 'viz' variable
//  interferes with the div id 'viz' (in the html), the following line is needed:
//  Note: this line does not appear in the tutorial videos but should still be used
//    and is in all of the included js files. 
var viz=null, workbook=null;
var myParameters = [];  // Each element has: key (menu name + menu entry (leve1) + menu entry (level2)), parameters Array of {param, value}
var currentSheetUrl = "";
var currentMenuPath = "";
var workBookId_prev = -1;
var params, paramEntry;
var wbName, paramMenu;
var txtSeparator = ' > '; // Separator for menu names

/*
 * include files templates inside a html TAG
 */
function include (fileName, idTag) {
	// Includes the contents of a fileName inside the tag idTag
	return $.get(fileName,function (data) {	$("#"+idTag).html(data);});
}

/*
 * Return Html sentence to structure the level 2 & 3 submenus  
 */
function constructSubmenus (workBookId, menuName, urlBase, subMenus) {
	// Submenus
	var subMenusArray = [];
	var menuOptions = "";
	var submenuOptions = "";
	var menuItem, submenuItem,sheetName;
	var subentries = {};
	var menuPath = "";
	
   	$.each(subMenus, function(key, value) {
		menuItem = key;
		subentries = value;
   		
		// If it's a simple submenu 
   		if (!(subentries instanceof Object)) {
			// Set menupath	   			
			menuPath = menuName + txtSeparator + menuItem;
			
			// Construct entry sentence
			if (menuItem=="-") { // A menu separator 
				menuOptions += '<li role="separator" class="divider"></li>';
			} else {
				menuOptions += '<li class="menu-item ">';
				menuOptions += '<a href="#"';
				menuOptions += " onClick='switchView(&quot;" + workBookId + "&quot;,&quot;" + urlBase + "&quot;,&quot;" + subentries + "&quot;,&quot;" + menuPath + "&quot;)'";
				menuOptions += '>';
				menuOptions += menuItem;
				menuOptions += '</a>';
				menuOptions += '</li>';
			}	
		} else {
		// If it's a submenu-entry with submenus
			// Construct submenus sentences
	   		submenuOptions = "";
		   	$.each(subentries, function(key2, value2) {
		   		submenuItem = key2;
		   		sheetName = value2;
		   		// Set menu path
		   		menuPath = menuName + txtSeparator + menuItem + txtSeparator + submenuItem;
		   		
			if (submenuItem=="-") { // A menu separator 
				submenuOptions += '<li role="separator" class="divider"></li>';
			} else {
		   		submenuOptions += '<li class="menu-item">';
		   		submenuOptions += '<a href="#"';
		   		submenuOptions += " onClick='switchView(&quot;" + workBookId + "&quot;,&quot;" + urlBase + "&quot;,&quot;" + sheetName + "&quot;,&quot;" + menuPath + "&quot;)'";
		   		submenuOptions += '>';
		   		submenuOptions += submenuItem;
		   		submenuOptions += '</a>';
		   		submenuOptions += '</li>';
		   	}
		   	});
		    // Construct menu entry sentence 	
			menuOptions += '<li class="menu-item dropdown dropdown-submenu">';
			menuOptions += '<a href="#" class="dropdown-toggle" data-toggle="dropdown">'; 
			menuOptions += menuItem; 
			menuOptions += '</a>';
			menuOptions += '<ul class="dropdown-menu">';
			menuOptions += submenuOptions;
		   	menuOptions += '</ul>';
		   	menuOptions += '</li>';
		}
   	});
	return '<ul class="dropdown-menu">' + menuOptions + '</ul>';
} // End constructSubmenus

/*
 * Find out if there is a 1st level menu already defined with name <menuName>
 * The 1st level menu must have submenus
 */
function menuInOptions (menuName, optionsString) {
	var regex = new RegExp ('<li class="menu-item dropdown"><a[^>]*>\\s*'+menuName+'.*?</a></li>');
	var arr = optionsString.match (regex); 
	return  arr != null;
}

/*
 * Add submenus to the 1st level existant menu with name <menuName>
 * The 1st level menu must have submenus already defined
 * here we just insert more submennus
 */
function insertSubmenusInOptions (menuName, submenus, optionsString) {
	var regex1 = new RegExp ('(<ul class="dropdown-menu[^"]*?">)(.+?)(</ul>)',"m");
	submenus = submenus.replace (regex1, "$2");
	
	var regex2 = new RegExp ('(<li class="menu-item dropdown"><a[^>]*>\\s*'+menuName+'.*?</a>\\s*<ul class="dropdown-menu[^"]*?">)\\s*(.+?)\\s*(</ul>\\s*</li>)');
	return optionsString.replace (regex2,"$1$2"+submenus+"$3");
}

/*
 * Write the html sentence to structure bootstrap multilevel submenus 
 */
function prepareMenuNavigation (myReports) {
	// Construct navigation menus in dash-template
	var wId = 0;
	var menuOptions = "";
	var submenuOptions;
	var menus, urlBase, submenus;
	var menuName = "";
	
	$.each(myReports, function(name, wkbook) {
		wId = wkbook.id;
		urlBase = wkbook.UrlBase;
		menus = wkbook.Menus;
		submenus = {};
		// Menus
		$.each(menus, function(key, value) {
			menuName = key;
			submenus = value;
			submenuOptions = "";
			if (submenus instanceof Object) {
				submenuOptions = constructSubmenus (wId, menuName,urlBase,submenus);
			}
			// If is a new menu-item, create entry
			if (!menuInOptions (menuName, menuOptions)) {
				// if it's a simple menu item
				if (submenuOptions == "") {
					menuOptions += '<li class="menu-item">';
					menuOptions += '<a href="#">';
					//menuOptions += menuName;
					menuOptions += '</a>';
					menuOptions += '</li>';	
				} else {
				// if it's a submenu entry
					menuOptions += '<li class="menu-item dropdown">';
					menuOptions += '<a href="#" class="dropdown-toggle" data-toggle="dropdown">';
					menuOptions += menuName;
					menuOptions += '<b class="caret"></b>';
					menuOptions += '</a>';
					menuOptions += submenuOptions;
					menuOptions += "</li>\n";
				}
			} else {
				// If the menu entry exists, then insert submenus to the same entry 
				menuOptions = insertSubmenusInOptions (menuName, submenuOptions, menuOptions);
			}
		}); // Endeach
	}); // End each
	$("#menuReportes").html(menuOptions);
} // End prepareMenuNavigation

function createTableauView (baseUrl, sheetName) {
	// Load tableau data and display in View
	var placeHolder = $('#tableauPlaceholder');
	var vizDiv = placeHolder[0]; // document.getElementById('tableauPlaceholder');
	var vizURL = baseUrl + sheetName;
	var def = $.Deferred(); // A promise object
	var options = {
		width: vizDiv.offsetWidth,
		height: vizDiv.offsetHeight,
		hideToolbar: false,
		hideTabs: true,
		onFirstInteractive: function () {
			viz = viztableau.viz;
			viz.addEventListener('tabswitch', function(event) {
				// $('#messageHeader').html(myReportsTitles[workBookId+"-"+event.getNewSheetName()]);
			});
			workbook = viz.getWorkbook();
			def.resolve();  // Promise accomplished
		}
	};
	var viztableau = new portalproduccion.VizTableau('tableauPlaceholder', vizURL, options);

	//viz = new tableauSoftware.Viz(vizDiv, vizURL, options);

	// Return a promise on "onFirstInteractive" function
	return def.promise(options.onFirstInteractive);
}

function switchView (workBookId, baseUrl, sheetName, menuPath) {
	var params; // Menu + Submenu + submenus
	var key, value;
	var pm, arr;
	
	if (workBookId != workBookId_prev) {
		if (viz) viz.dispose();
        pm = createTableauView(baseUrl, sheetName);
    } else { 
        pm = workbook.activateSheetAsync(sheetName);
	}
	
	$.when (pm)
	.then (function(){
	    // Set current URL for exporting the image/pdf    
	    currentSheetUrl = baseUrl + sheetName;
	    // Set previus woorkBookId
	    workBookId_prev = workBookId;    
	    // Display export dropdown    
	    $('#exportZone').show();
	
		// TABLEAU PARAMETRS
		// Check if current menu entry have associated parameters to set
		console.log ("menuPath : "+menuPath);
		params = myParameters[menuPath];
		if (params instanceof Array) {
			$.each(params, function (index, pairValue) {
				arr = pairValue.split(","); 
				key = arr[0];
				value = arr[1];
			 	workbook.changeParameterValueAsync(key, value)
			    .then(function() {console.log ('Param [' + key + ']= ' + value );})
			    .otherwise(function(err) { console.log ('Param set failed: ' + err);});
			});
		}
		// escribe la ruta actual de los menus
		$('#messageHeader').html(menuPath);
		currentMenuPath = menuPath;
	});
} // End switchView

/*
 * EXPORT DROPDOWN for export dropdown button bellow tableu placeholder
 * It needs to take into account the tableau Parameters
*/
function exportActions () { 
	$("#export li").on('click',function(){
		if (currentSheetUrl!="") {
			// set tableau param as a query string
			// ?PARAM.Piramide=Rol%20Indra
			var queryString = "";
			if (currentMenuPath!="") {
				var arr, key, value;
				// Check if current menu entry have associated parameters
				params = myParameters[currentMenuPath];
				if (params instanceof Array) {
					$.each(params, function (index, pairValue) {
						queryString += (queryString=="" ? "?" : "&"); 
						arr = pairValue.split(","); 
						key = arr[0];
						value = arr[1];
						queryString += key + "=" + encodeURIComponent(value); 
					});
				}
			}	
			window.open(currentSheetUrl+"."+$(this).attr('name')+queryString,'_blank');
		}
	});
}

/*
 * Submenu actions needed to support multilevel submenus in bootstrap 3.
 * Source: http://stackoverflow.com/questions/18023493/bootstrap-3-dropdown-sub-menu-missing 
 */
function submenuActions () {
	$('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
	    // Avoid following the href location when clicking
	    event.preventDefault(); 
	    // Avoid having the menu to close when clicking
	    event.stopPropagation(); 
	    // If a menu is already open we close it
	    //$('ul.dropdown-menu [data-toggle=dropdown]').parent().removeClass('open');
	    // opening the one you clicked on
	    $(this).parent().addClass('open');
	
	    var menu = $(this).parent().find("ul");
	    var menupos = menu.offset();
	  
	    if ((menupos.left + menu.width()) + 30 > $(window).width()) {
	        var newpos = - menu.width();      
	    } else {
	        var newpos = $(this).parent().width();
	    }
	    menu.css({ left:newpos });
	
	});
} // end submenuActions

var dParameters = null;
var dMenus = null;
$(document).ready (function() {
   $.when(
		include ("./templates/header-template.html","header-template"),
		include ("./templates/nav-template.html","nav-template"),
		include ("./templates/dash-template.html","dash-template"),
		include ("./templates/footer-template.html","footer-template")
   ).done (function () {
   	    $('#exportZone').hide();

		// Load json data
		$.when (
			$.getJSON("./json/reportsInfo.json", function(dataMenus) {
				dMenus = dataMenus;
			}),
			$.getJSON("./json/parameters.json", function(dataParameters) {
				dParameters = dataParameters;
			})
		).always (function () {
			if (!dMenus) {
				alert("Server communication failure. please refresh your browser and try again.");
				console.log('Error al leer el Json de Menus');
				exit;
			}
			
			// Get TABLEAU Parameters
			if (dParameters) {
				// Build an array of parameters for each param entry
				$.each (dParameters, function(wbName, wkbook) {
					params = wkbook.Parameters;
					paramEntry = {};
					$.each(params, function(paramMenu, paramEntry) {
						// Converts paramMenu in menuPath entries
						paramMenu = paramMenu.replace(/@/g,txtSeparator);
						myParameters[paramMenu] = [];
						$.each(paramEntry, function(key, value) {
							myParameters[paramMenu].push(key+","+value);
						});
					});
				});	
			}	
			 //console.log (JSON.stringify(data));
			prepareMenuNavigation(dMenus);
			 
			exportActions ();

			submenuActions();
		}); // End Then read json
		
	}) // Done Doc ready
	.fail(function(jqXHR, textStatus, errorThrown) {
		if (textStatus == 'timeout') {
			console.log('The server is not responding');
		}
		if (textStatus == 'error') {
			console.log('Atencion: '+errorThrown);
		}
	});
});
